/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/06 17:19:39 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/06 18:11:07 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *new_list;

	if (lst == NULL)
		return (NULL);
	new_list = f(lst);
	if (new_list != NULL)
	{
		new_list = f(lst);
		new_list->next = ft_lstmap(lst->next, f);
	}
	return (new_list);
}
