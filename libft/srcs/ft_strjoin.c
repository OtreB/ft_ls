/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 21:14:56 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/05 18:42:33 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(const char *s1, const char *s2)
{
	char *str;

	if (s1 != NULL && s2 != NULL)
	{
		if ((str = (char*)malloc(sizeof(*str) * (ft_strlen(s1) +
							ft_strlen(s2) + 1)))
					== NULL)
			return (NULL);
		ft_strcpy(str, s1);
		ft_strcpy(&str[ft_strlen(s1)], s2);
		str[ft_strlen(s1) + ft_strlen(s2)] = '\0';
		return (str);
	}
	return (NULL);
}
