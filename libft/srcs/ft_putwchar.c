/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/22 15:12:57 by cumberto          #+#    #+#             */
/*   Updated: 2017/03/24 05:58:36 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putwchar(wchar_t chr)
{
	if (chr < 0x7f)
		ft_putchar(chr);
	else if (chr < 0x7ff)
	{
		ft_putchar((chr >> 6) | 0xc0);
		ft_putchar((chr | 0x80) & 0xbf);
	}
	else if (chr < 0xffff)
	{
		ft_putchar((chr >> 12) | 0xe0);
		ft_putchar(((chr >> 6) | 0x80) & 0xbf);
		ft_putchar((chr | 0x80) & 0xbf);
	}
	else
	{
		ft_putchar(((chr >> 18) | 0xf0) & 0xf7);
		ft_putchar(((chr >> 12) | 0x80) & 0xbf);
		ft_putchar(((chr >> 6) | 0x80) & 0xbf);
		ft_putchar((chr | 0x80) & 0xbf);
	}
}
