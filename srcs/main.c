#include "ft_ls.h"

char	*ft_strdup_m(const char *str)
{
	char	*cpy;
	int		i;

	if ((cpy = (char*)malloc(sizeof(char) * ft_strlen(str) + 1)) == NULL)
		return (NULL);
	i = 0;
	while (str[i])
	{
		cpy[i] = str[i];
		i++;
	}
	cpy[i] = '\0';
	return (cpy);
}

void	find_flags(const char *str)
{
	int ind;

	ind = 1;
	while (str[ind] != '\0')
	{
		if (str[ind] == 'R')
			g_R = 1;
		else if (str[ind] == 'r')
			g_r = 1;
		else if (str[ind] == 'l')
			g_l = 1;
		else if (str[ind] == 't')
			g_t = 1;
		else if (str[ind] == 'a')
			g_a = 1;
		else
			break ;
		ind++;
	}
}

int		read_args(t_list **start, int ac, const char *av[])
{
	int ind;
	int size;
	t_cdir elem;

	size = 1;
	ind = 1;
	while (ind < ac && av[ind][0] == '-' && av[ind][1] != '\0')
	{
		find_flags(av[ind]);
		ind++;
	}
	if (ind == ac)
	{
		elem.name = ft_strdup_m(".");
		elem.path = ft_strdup_m(".");
		*start = ft_lstnew(&elem, sizeof(t_cdir));
		return (1);
	}
	elem.name = ft_strdup_m(av[ind]);
	elem.path = ft_strdup_m(av[ind]);
	*start = ft_lstnew(&elem, sizeof(t_cdir));
	ind++;
	while (ind < ac)
	{
		ft_bzero(&elem, sizeof(t_cdir));
		size++;
		elem.name = ft_strdup_m(av[ind]);
		elem.path = ft_strdup_m(av[ind]);
		ft_lstadd(start, ft_lstnew(&elem, sizeof(t_cdir)));
		ind++;
	}
	return (size);
}

int main(int argc, const char *argv[])
{
	t_list *elem;
	int size;

	elem = NULL;
	size = read_args(&elem, argc, argv);
	elem = sort(elem);
	ft_ls(elem, size);
	return 0;
}
