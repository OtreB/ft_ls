#include "ft_ls.h"

int		sort_alphabet(char *s1, char *s2)
{
	int ind1;
	int ind2;
	int num1;
	int num2;

	ind1 = 0;
	ind2 = 0;
	num1 = 0;
	num2 = 0;
	while (s1[ind1] != '\0' && s2[ind2] != '\0')
	{
		while (ft_isalpha(s1[ind1]) != 1  && ft_isdigit(s1[ind1]) != 1 && s1[ind1] != '\0')
			ind1++;
		while (ft_isalpha(s2[ind2]) != 1  && ft_isdigit(s2[ind2]) != 1 && s2[ind2] != '\0')
			ind2++;
		if (s1[ind1] <= 90)
			num1 = s1[ind1] >= 65 ? 32 : 0;
		if (s2[ind2] <= 90)
			num2 = s2[ind2] >= 65 ? 32 : 0;
		if((s1[ind1] + num1) != (s2[ind2] + num2))
			break;
		ind1++;
		ind2++;
	}
	return ((s1[ind1] + num1) - (s2[ind2] + num2));
}

t_list *sort(t_list *elem)
{
	t_list *tmp1;
	t_list *tmp2;
	t_cdir *tmp3;
	int cnt;

	cnt = 1;
	while(cnt != 0)
	{
		tmp1 = elem;
		cnt = 0;
		while(tmp1->next != NULL)
		{
			tmp2 = tmp1->next;
			if (sort_alphabet(((t_cdir*)tmp1->content)->name,((t_cdir*)tmp2->content)->name) > 0)
			{
				tmp3 = tmp1->content;
				tmp1->content = tmp2->content;
				tmp2->content = tmp3;
				cnt++;
			}
			tmp1 = tmp1->next;
		}
	}
	return (elem);
}
