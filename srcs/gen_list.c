#include "ft_ls.h"

void	free_list(t_list *elem)
{
	t_list *tmp;

	while (elem != NULL)
	{
		free(((t_cdir*)elem->content)->name);
		free(((t_cdir*)elem->content)->path);
		tmp = elem;
		elem = elem->next;
		free(tmp);
	}
}
