/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_test.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 15:33:38 by cumberto          #+#    #+#             */
/*   Updated: 2017/04/03 17:05:32 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_list	*ignore_dot(t_list *elem)
{
	if (((t_cdir*)elem->content)->name[0] == '.')
	{
		if (((t_cdir*)elem->content)->name[1] == '\0')
			elem = elem->next;
		if (((t_cdir*)elem->content)->name[0] == '.')
			if (((t_cdir*)elem->content)->name[1] == '.')
				if (((t_cdir*)elem->content)->name[2] == '\0')
					elem = elem->next;

	}
	return (elem);
}

void	print_test(t_list *elem, int size, char *path)
{
	if (size > 1 || g_R == 1)
		printf("%s:\n", path);
	while (elem != NULL)
	{
		if (g_a != 1)
			while(elem != NULL && ((t_cdir*)elem->content)->name[0] == '.')
				elem = elem->next;
		if (elem == NULL)
			break ;
		printf("%s\n",T_E->name);
		elem = elem->next;
	}
	printf("\n");
}

char *add_path(char *old_path, char *new_path)
{
	char	*path;

	if (ft_strcmp(old_path, new_path) == 0)
		return(new_path);
	path = ft_strjoin(ft_strjoin(old_path, "/"), new_path);
	return (path);
}

int		store_info(t_list **start, DIR *pDIR, char *old_path)
{
	struct dirent *direct;
	t_cdir file;
	int size;

	if ((direct = readdir(pDIR)) == NULL)
		return (0);
	size = 1;
	file.name = ft_strdup(direct->d_name);
	file.path = add_path(old_path, file.name);
	*start = ft_lstnew(&file, sizeof(file));
	while((direct = readdir(pDIR)) != NULL)
	{
		size++;
		file.name = ft_strdup(direct->d_name);
		file.path = add_path(old_path, file.name);
		ft_lstadd(start, ft_lstnew(&file, sizeof(t_cdir)));
	}
	return (size);
}

int		ft_ls(t_list *elem, int size)
{
	DIR *pDIR;
	t_list *start;
	int new_size;

	if (elem == NULL)
		return (0);
	if ((pDIR = opendir(((t_cdir*)elem->content)->path)) == NULL)
		return (0);
	new_size = store_info(&start,pDIR, ((t_cdir*)elem->content)->path);
	start = sort(start);
	print_test(start, size, ((t_cdir*)elem->content)->path);

	if (g_R == 1)
	{
		while (start != NULL)
		{
			if((start = ignore_dot(start)) != NULL)
			{
				if (g_a != 1)
					while (start != NULL && ((t_cdir*)start->content)->name[0] == '.')
						start = start->next;
			ft_ls(start, new_size--);
			}
			else
				break ;
			if (start != NULL)
				start = start->next;
		}
		free_list(start);
		return (0);
	}
	if ((elem = elem->next) != NULL)
		ft_ls(elem, size--);
	return (0);
}

void error(int err_num)
{
	if (err_num == 0)
		ft_putstr("missing folder name");
	if (err_num == 1)
		printf("error!!!");
	exit(0);
}
