/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 16:50:14 by cumberto          #+#    #+#             */
/*   Updated: 2017/04/03 17:07:11 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

#include "../libft/includes/libft.h"
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

#define T_E ((t_cdir*)elem->content)
#define T_S ((t_cdir*)start->content)

typedef struct	s_cdir
{
	char	*name;
	char	*path;
}				t_cdir;

int g_a;
int g_l;
int g_R;
int g_r;
int g_t;

int			ft_ls(t_list *elem, int size);
t_list		*sort(t_list *elem);
int			list_size(t_list *elem);
void		free_list(t_list *elem);

#endif
