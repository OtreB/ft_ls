# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cumberto <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/03 16:30:13 by cumberto          #+#    #+#              #
#    Updated: 2017/04/03 17:20:07 by cumberto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC_NAME = main.c \
		   sort.c \
		   ft_ls.c \
		   gen_list.c \

OBJ_NAME = $(SRC_NAME:.c=.o)

SRC_PATH = srcs

OBJ_PATH = obj

SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))

NAME = ft_ls

CFLAGS = -Wall -Wextra # -Werror

INCLUDES = -Iincludes -Ilibft/includes

all: $(NAME)

$(NAME): libft $(OBJ)
	gcc $(CFLAGS) $(OBJ) $(INCLUDES) ./libft/libft.a -o $(NAME)

libft:
	make -C ./libft/

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	/bin/mkdir -p $(OBJ_PATH)
	 gcc $(CFLAGS) -Iincludes -c $< -o $@
#	@ printf " [-]compiling  src -> "
#	@ printf "$<                                           \r"

clean:
	make -C ./libft clean
	rm -Rf $(OBJ_PATH)

fclean: clean
	make -C ./libft fclean
	@rm ./ft_ls

re: fclean all

test: libft $(OBJ)
	gcc -g $(CFLAGS) $(SRC) $(INCLUDES) ./libft/libft.a -o $(NAME)
